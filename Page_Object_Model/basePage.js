import { Selector, t } from "testcafe";
import xpathSelector from "../utils/xpath-selector";
class Page {
    constructor() {
        //Login page
        this.accountButton = Selector('.arrowIcon');
        this.inputPhoneNumber = Selector("input[name='tel']");
        this.btnTiepTuc = Selector('button').withText('Tiếp Tục');
        this.inputPassword = Selector("input[type='password']");
        this.btnDangNhap = Selector('button').withText('Đăng Nhập');
        // Search item
        this.inputSearchTextBox = Selector("input[data-view-id='main_search_form_input']");
        this.btnSearch = Selector('button').withText('Tìm Kiếm');
        this.btnPaganition = Selector("a[data-view-id='product_list_pagination_item']")
        this.lastItem = xpathSelector('//*[@id="__next"]/div[1]/main/div[2]/div[1]/div[2]/div[1]/div[2]/a[48]');
        this.itemTitle = xpathSelector("//*[@ class='title']")

    }
    async inputPhoneNumberFunc(phoneNumber) {
        await t
            .typeText(this.inputPhoneNumber, phoneNumber)
    }

    async inputPasswordFunc(password) {
        await t
            .typeText(this.inputPassword, password)
    }

    async paginationFunc(pageNumber) {
        await t
            .click(this.btnPaganition.withText(pageNumber))
    }

    async clickLastItem() {
        await t
            .click(this.lastItem)
    }

    async assertItemTitle(itemTitle) {
        await t
            .expect(this.itemTitle.innerText).eql(itemTitle)
    }
}


export default new Page();
