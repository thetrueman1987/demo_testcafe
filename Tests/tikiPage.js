import { Selector } from 'testcafe'
import login from '../Page_Object_Model/basePage'
import search from '../Page_Object_Model/basePage'
import { ClientFunction } from 'testcafe';

const scrollBy1 = ClientFunction(() => {
    window.scrollBy(100, 6000);
});

const itemTitle = 'Laptop HP Envy 13 ba1030TU i7 1165G7/8GB/512GB/13.3"F/OfficeHS/Win10/(2K0B6PA)/Vàng - Hàng chính hãng';
fixture('testcafe tiki demo')
    .page('https://tiki.vn/');


test('Resize Window test', async t => {
    await t
        .resizeWindowToFitDevice('iphonexr')
        .maximizeWindow();
});

test('Login function test', async t => {
    await t
        .click(login.accountButton);

    await login.inputPhoneNumberFunc('0966742887')

    await t
        .click(login.btnTiepTuc)

    await login.inputPasswordFunc('Anhvutran@210821')

    await t
        .click(login.btnDangNhap)
        .wait(5000)
});

test('Test search function, scroll , change pagination and click last item', async t => {
    //Search function
    await t
        .maximizeWindow()
        .typeText(search.inputSearchTextBox, 'laptop')
        .click(search.btnSearch)
        .wait(3000)
    //Scroll    
    await scrollBy1();
    await t
        .wait(3000)
    //Change pagination     
    await search.paginationFunc('2')
    await t
        .wait(3000)
    //Click last Item    
    await search.clickLastItem();
    await t
        .wait(2000)
    await search.assertItemTitle(itemTitle);
});